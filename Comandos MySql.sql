create database practica_bitacora;
use practica_bitacora;

create table Carrera (
	id int not null auto_increment primary key,
    descripcion varchar(250) not null
);

create table Alumno (
	matricula int not null primary key,
    paterno varchar(200) not null,
    materno varchar(200) not null,
    nombre varchar(200) not null,
    carrera int not null,
    fecha_ingreso date not null,
    foreign key (carrera) references Carrera(id)
);

create table Bitacora (
	id int not null auto_increment primary key,
    usuario varchar(200) not null,
    fecha_hora timestamp not null default current_timestamp,
    operacion varchar(200) not null,
    campo varchar(200) not null,
    viejo varchar(200) not null,
    nuevo varchar(200) not null
);

insert into Carrera values (null, 'Ing. Sistemas Computacionales');

create trigger AlInsertar
	after insert on Alumno
	for each row
    insert into bitacora values (null, "userX", current_timestamp(), "Insert", "matricula", "", new.matricula);

delimiter ///
create trigger AlActualizar
	after update on Alumno
    for each row
    begin
		if old.paterno <> new.paterno then
			insert into Bitacora values (null, "userX", current_timestamp(), "Update", "paterno", old.paterno, new.paterno);
        end if;
        if old.materno<> new.materno then
			insert into Bitacora values (null, "userX", current_timestamp(), "Update", "materno", old.materno, new.materno);
        end if;
        if old.nombre <> new.nombre then
			insert into Bitacora values (null, "userX", current_timestamp(), "Update", "nombre", old.nombre, new.nombre);
        end if;
        if old.carrera <> new.carrera then
			insert into Bitacora values (null, "userX", current_timestamp(), "Update", "carrera", old.carrera, new.carrera);
        end if;
        if old.fecha_ingreso <> new.fecha_ingreso then
			insert into Bitacora values (null, "userX", current_timestamp(), "Update", "fecha_ingreso", old.fecha_ingreso, new.fecha_ingreso);
        end if;
    end;
/// 
delimiter ;
    
create trigger AlEliminar
	after delete on Alumno
    for each row
    insert into bitacora values (null, "userX", current_timestamp(), "Delete", "matricula", old.matricula, "");
