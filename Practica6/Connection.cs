﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace Practica6 {
    public class Connection {
        private readonly MySqlConnection connection;
        private readonly MySqlCommand command;
        private MySqlDataReader reader;

        public Connection(string connectionString) {
            connection = new MySqlConnection(connectionString);
            command = connection.CreateCommand();
        }

        public void Connect() {
            try {
                connection.Open();
            } catch (Exception) {
                throw new Exception("Hubo un error");
            }
        }

        public void Disconnect() {
            try {
                connection.Close();
            } catch (Exception) {
                throw new Exception("Hubo un error");
            }
        }

        public DataTable GetStudents() {
            DataTable dataTable = new DataTable();

            command.CommandText = "SELECT Alumno.matricula, Alumno.paterno, Alumno.materno, Alumno.nombre, Carrera.descripcion, Alumno.fecha_ingreso FROM Alumno INNER JOIN Carrera ON Alumno.Carrera = Carrera.Id;";

            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(command);

            dataAdapter.Fill(dataTable);

            return dataTable;
        }

        public DataTable GetLog() {
            DataTable dataTable = new DataTable();

            command.CommandText = "SELECT * FROM Bitacora;";

            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(command);

            dataAdapter.Fill(dataTable);

            return dataTable;
        }

        public List<string> GetCareers() {
            List<string> careers = new List<string>();

            command.CommandText = "SELECT descripcion FROM Carrera;";

            reader = command.ExecuteReader();

            while (reader.Read()) {
                careers.Add(reader.GetValue(0).ToString());
            }
            reader.Close();

            return careers;
        }

        public void SaveStudent(
            string id,
            string pLastName,
            string mLastName,
            string name,
            string career,
            string dateAdmission
        ) {
            string careerDescription = "";
            int studentExists = 0;

            command.CommandText = string.Format("SELECT id FROM Carrera WHERE descripcion = \"{0}\";", career);
            reader = command.ExecuteReader();

            while (reader.Read()) {
                careerDescription = reader.GetValue(0).ToString();
            }
            reader.Close();

            command.CommandText = string.Format("SELECT COUNT(*) FROM Alumno WHERE matricula = {0};", id);
            reader = command.ExecuteReader();

            while (reader.Read()) {
                studentExists = int.Parse(reader.GetValue(0).ToString());
            }
            reader.Close();

            if (studentExists > 0) {
                command.CommandText = string.Format("UPDATE Alumno SET matricula = \"{0}\", paterno = \"{1}\", materno = \"{2}\", nombre = \"{3}\", carrera = {4}, fecha_ingreso = \"{5}\" WHERE matricula = \"{6}\";", id, pLastName, mLastName, name, careerDescription, dateAdmission, id);
                command.ExecuteNonQuery();
            } else {
                command.CommandText = string.Format("INSERT INTO Alumno VALUES (\"{0}\", \"{1}\", \"{2}\", \"{3}\", {4}, \"{5}\");", id, pLastName, mLastName, name, careerDescription, dateAdmission);
                command.ExecuteNonQuery();
            }
        }

        public int DeleteStudent(string id) {
            int result;
            
            try {
                command.CommandText = string.Format("DELETE FROM Alumno WHERE matricula = {0}", id);
                result = command.ExecuteNonQuery();
            } catch(Exception) {
                throw new Exception("Hubo un error");
            }

            return result;
        }
    }
}
