﻿namespace Practica6 {
    partial class FormDelete {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.buttonDeleteStudent = new System.Windows.Forms.Button();
            this.labelIdDelete = new System.Windows.Forms.Label();
            this.textBoxIdDelete = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonDeleteStudent
            // 
            this.buttonDeleteStudent.Location = new System.Drawing.Point(38, 65);
            this.buttonDeleteStudent.Name = "buttonDeleteStudent";
            this.buttonDeleteStudent.Size = new System.Drawing.Size(215, 23);
            this.buttonDeleteStudent.TabIndex = 2;
            this.buttonDeleteStudent.Text = "Eliminar alumno";
            this.buttonDeleteStudent.UseVisualStyleBackColor = true;
            this.buttonDeleteStudent.Click += new System.EventHandler(this.buttonDeleteStudent_Click);
            // 
            // labelIdDelete
            // 
            this.labelIdDelete.AutoSize = true;
            this.labelIdDelete.Location = new System.Drawing.Point(35, 31);
            this.labelIdDelete.Name = "labelIdDelete";
            this.labelIdDelete.Size = new System.Drawing.Size(53, 13);
            this.labelIdDelete.TabIndex = 0;
            this.labelIdDelete.Text = "Matricula:";
            // 
            // textBoxIdDelete
            // 
            this.textBoxIdDelete.Location = new System.Drawing.Point(91, 28);
            this.textBoxIdDelete.Name = "textBoxIdDelete";
            this.textBoxIdDelete.Size = new System.Drawing.Size(162, 20);
            this.textBoxIdDelete.TabIndex = 1;
            // 
            // FormDelete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 103);
            this.Controls.Add(this.textBoxIdDelete);
            this.Controls.Add(this.labelIdDelete);
            this.Controls.Add(this.buttonDeleteStudent);
            this.Name = "FormDelete";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Eliminar alumno";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonDeleteStudent;
        private System.Windows.Forms.Label labelIdDelete;
        private System.Windows.Forms.TextBox textBoxIdDelete;
    }
}