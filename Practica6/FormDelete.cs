﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica6 {
    public partial class FormDelete : Form {
        private Connection mysqlConnection;
        private Form1 form1;

        public FormDelete(Form1 form1, Connection mysqlConnection) {
            InitializeComponent();

            this.mysqlConnection = mysqlConnection;
            this.form1 = form1;
        }

        private void buttonDeleteStudent_Click(object sender, EventArgs e) {
            string id = textBoxIdDelete.Text;

            if (isNumeric(id)) {
                try {
                    int result = mysqlConnection.DeleteStudent(id);
                    if (result > 0) {
                        form1.Refresh();
                        
                        Close();
                    } else {
                        MessageBox.Show("El usuario no existe");
                    }
                } catch (Exception) {
                    MessageBox.Show("Hubo un error");
                }
            } else {
                MessageBox.Show("La matrícula tiene que ser numérica");
            }
        }

        public bool isNumeric(string id) {
            int numeric;

            return int.TryParse(id, out numeric);
        }
    }
}
