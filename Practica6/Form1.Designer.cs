﻿namespace Practica6 {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.labelId = new System.Windows.Forms.Label();
            this.labelPLastName = new System.Windows.Forms.Label();
            this.labelMLastName = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelCareer = new System.Windows.Forms.Label();
            this.labelDateAdmission = new System.Windows.Forms.Label();
            this.textBoxPLastName = new System.Windows.Forms.TextBox();
            this.textBoxMLastName = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.comboBoxCareer = new System.Windows.Forms.ComboBox();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonClean = new System.Windows.Forms.Button();
            this.dataGridViewLog = new System.Windows.Forms.DataGridView();
            this.labelLog = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLog)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(12, 12);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.Size = new System.Drawing.Size(969, 150);
            this.dataGridView.TabIndex = 9;
            this.dataGridView.SelectionChanged += new System.EventHandler(this.dataGridView_SelectionChanged);
            // 
            // textBoxId
            // 
            this.textBoxId.Location = new System.Drawing.Point(12, 204);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.Size = new System.Drawing.Size(200, 20);
            this.textBoxId.TabIndex = 1;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(12, 451);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(58, 23);
            this.buttonSave.TabIndex = 7;
            this.buttonSave.Text = "Guardar";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // labelId
            // 
            this.labelId.AutoSize = true;
            this.labelId.Location = new System.Drawing.Point(9, 188);
            this.labelId.Name = "labelId";
            this.labelId.Size = new System.Drawing.Size(55, 13);
            this.labelId.TabIndex = 0;
            this.labelId.Text = "Matrícula:";
            // 
            // labelPLastName
            // 
            this.labelPLastName.AutoSize = true;
            this.labelPLastName.Location = new System.Drawing.Point(9, 232);
            this.labelPLastName.Name = "labelPLastName";
            this.labelPLastName.Size = new System.Drawing.Size(66, 13);
            this.labelPLastName.TabIndex = 0;
            this.labelPLastName.Text = "Ap. Paterno:";
            // 
            // labelMLastName
            // 
            this.labelMLastName.AutoSize = true;
            this.labelMLastName.Location = new System.Drawing.Point(12, 276);
            this.labelMLastName.Name = "labelMLastName";
            this.labelMLastName.Size = new System.Drawing.Size(68, 13);
            this.labelMLastName.TabIndex = 0;
            this.labelMLastName.Text = "Ap. Materno:";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(12, 320);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(47, 13);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Nombre:";
            // 
            // labelCareer
            // 
            this.labelCareer.AutoSize = true;
            this.labelCareer.Location = new System.Drawing.Point(12, 364);
            this.labelCareer.Name = "labelCareer";
            this.labelCareer.Size = new System.Drawing.Size(44, 13);
            this.labelCareer.TabIndex = 0;
            this.labelCareer.Text = "Carrera:";
            // 
            // labelDateAdmission
            // 
            this.labelDateAdmission.AutoSize = true;
            this.labelDateAdmission.Location = new System.Drawing.Point(9, 409);
            this.labelDateAdmission.Name = "labelDateAdmission";
            this.labelDateAdmission.Size = new System.Drawing.Size(92, 13);
            this.labelDateAdmission.TabIndex = 0;
            this.labelDateAdmission.Text = "Fecha de ingreso:";
            // 
            // textBoxPLastName
            // 
            this.textBoxPLastName.Location = new System.Drawing.Point(12, 248);
            this.textBoxPLastName.Name = "textBoxPLastName";
            this.textBoxPLastName.Size = new System.Drawing.Size(200, 20);
            this.textBoxPLastName.TabIndex = 2;
            // 
            // textBoxMLastName
            // 
            this.textBoxMLastName.Location = new System.Drawing.Point(12, 292);
            this.textBoxMLastName.Name = "textBoxMLastName";
            this.textBoxMLastName.Size = new System.Drawing.Size(200, 20);
            this.textBoxMLastName.TabIndex = 3;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(12, 336);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(200, 20);
            this.textBoxName.TabIndex = 4;
            // 
            // comboBoxCareer
            // 
            this.comboBoxCareer.FormattingEnabled = true;
            this.comboBoxCareer.Location = new System.Drawing.Point(12, 380);
            this.comboBoxCareer.Name = "comboBoxCareer";
            this.comboBoxCareer.Size = new System.Drawing.Size(200, 21);
            this.comboBoxCareer.TabIndex = 5;
            this.comboBoxCareer.Text = "--Seleccionar--";
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Location = new System.Drawing.Point(12, 425);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker.TabIndex = 6;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(83, 451);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(58, 23);
            this.buttonDelete.TabIndex = 8;
            this.buttonDelete.Text = "Eliminar";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonClean
            // 
            this.buttonClean.Location = new System.Drawing.Point(154, 451);
            this.buttonClean.Name = "buttonClean";
            this.buttonClean.Size = new System.Drawing.Size(58, 23);
            this.buttonClean.TabIndex = 10;
            this.buttonClean.Text = "Limpiar";
            this.buttonClean.UseVisualStyleBackColor = true;
            this.buttonClean.Click += new System.EventHandler(this.buttonClean_Click);
            // 
            // dataGridViewLog
            // 
            this.dataGridViewLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLog.Location = new System.Drawing.Point(237, 204);
            this.dataGridViewLog.Name = "dataGridViewLog";
            this.dataGridViewLog.Size = new System.Drawing.Size(744, 270);
            this.dataGridViewLog.TabIndex = 11;
            // 
            // labelLog
            // 
            this.labelLog.AutoSize = true;
            this.labelLog.Location = new System.Drawing.Point(567, 188);
            this.labelLog.Name = "labelLog";
            this.labelLog.Size = new System.Drawing.Size(25, 13);
            this.labelLog.TabIndex = 12;
            this.labelLog.Text = "Log";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 480);
            this.Controls.Add(this.labelLog);
            this.Controls.Add(this.dataGridViewLog);
            this.Controls.Add(this.buttonClean);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.comboBoxCareer);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.textBoxMLastName);
            this.Controls.Add(this.textBoxPLastName);
            this.Controls.Add(this.labelDateAdmission);
            this.Controls.Add(this.labelCareer);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelMLastName);
            this.Controls.Add(this.labelPLastName);
            this.Controls.Add(this.labelId);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.textBoxId);
            this.Controls.Add(this.dataGridView);
            this.Name = "Form1";
            this.Text = "Bitácora";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLog)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label labelId;
        private System.Windows.Forms.Label labelPLastName;
        private System.Windows.Forms.Label labelMLastName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelCareer;
        private System.Windows.Forms.Label labelDateAdmission;
        private System.Windows.Forms.TextBox textBoxPLastName;
        private System.Windows.Forms.TextBox textBoxMLastName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.ComboBox comboBoxCareer;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonClean;
        private System.Windows.Forms.DataGridView dataGridViewLog;
        private System.Windows.Forms.Label labelLog;
    }
}

