﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Practica6 {
    public partial class Form1 : Form {
        public readonly Connection mysqlConnection;
        private readonly string connectionString;
        private FormDelete formDelete;

        public Form1() {
            InitializeComponent();

            connectionString = string.Format("Server=localhost;Uid=luis;Password=1084;Database=practica_bitacora;");
            mysqlConnection = new Connection(connectionString);
        }

        private void Form1_Load(object sender, EventArgs e) {
            mysqlConnection.Connect();
            dataGridView.DataSource = mysqlConnection.GetStudents();
            comboBoxCareer.DataSource = mysqlConnection.GetCareers();
            Refresh();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            mysqlConnection.Disconnect();
        }

        private void buttonSave_Click(object sender, EventArgs e) {
            string id = textBoxId.Text;
            string pLastName = textBoxPLastName.Text;
            string mLastName = textBoxMLastName.Text;
            string name = textBoxName.Text;
            string career = comboBoxCareer.SelectedItem.ToString();
            string admission = dateTimePicker.Value.ToString("yyyy-MM-dd");

            if (
                string.IsNullOrWhiteSpace(id) ||
                string.IsNullOrWhiteSpace(pLastName) ||
                string.IsNullOrWhiteSpace(mLastName) ||
                string.IsNullOrWhiteSpace(name) ||
                string.IsNullOrWhiteSpace(career) ||
                string.IsNullOrWhiteSpace(admission)
            ) {
                MessageBox.Show("Los campos no pueden quedar vacíos");
            } else if (!isNumeric(id)) {
                MessageBox.Show("La matrícula tiene que ser numérica");
            } else {
                mysqlConnection.SaveStudent(id, pLastName, mLastName, name, career, admission);
                Refresh();
                Clean();
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            formDelete = new FormDelete(this, mysqlConnection);
            formDelete.Show();
        }

        private void buttonClean_Click(object sender, EventArgs e) {
            Clean();
        }

        private void dataGridView_SelectionChanged(object sender, EventArgs e) {
            int index;
            foreach (DataGridViewRow row in dataGridView.SelectedRows) {
                index = comboBoxCareer.FindString(row.Cells[4].Value.ToString());

                textBoxId.Text = row.Cells[0].Value.ToString();
                textBoxPLastName.Text = row.Cells[1].Value.ToString();
                textBoxMLastName.Text = row.Cells[2].Value.ToString();
                textBoxName.Text = row.Cells[3].Value.ToString();
                comboBoxCareer.SelectedIndex = index;
            }
        }

        public void Refresh() {
            dataGridView.DataSource = mysqlConnection.GetStudents();
            dataGridViewLog.DataSource = mysqlConnection.GetLog();
        }

        private void Clean() {
            textBoxId.Text = "";
            textBoxPLastName.Text = "";
            textBoxMLastName.Text = "";
            textBoxName.Text = "";
        }

        public bool isNumeric(string id) {
            int numeric;

            return int.TryParse(id, out numeric);
        }
    }
}
